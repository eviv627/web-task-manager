package ru.evseenko.tm.entity;


import org.jetbrains.annotations.NotNull;

public enum Role { ADMIN("ADMIN"), USER("USER");
    @NotNull
    private String name;

    Role(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getName() {
        return name;
    }
}
