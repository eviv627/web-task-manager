package ru.evseenko.tm.entity.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.api.entity.Identifiable;
import ru.evseenko.tm.entity.Role;

import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;

@Setter
@Getter
@XmlSeeAlso({Role.class})
@NoArgsConstructor
public class UserDTO implements Serializable, Identifiable {

    @Nullable
    private String id;

    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private Role role;

    @Override
    public String toString() {
        return "UserDTO{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", role=" + role +
                '}';
    }
}
