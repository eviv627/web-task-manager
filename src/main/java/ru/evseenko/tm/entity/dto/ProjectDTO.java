package ru.evseenko.tm.entity.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.api.entity.DomainDTO;
import ru.evseenko.tm.api.entity.SortableEntity;
import ru.evseenko.tm.entity.EntityUtill;
import ru.evseenko.tm.entity.Status;

import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@XmlSeeAlso({Status.class})
@NoArgsConstructor
public class ProjectDTO implements SortableEntity, Serializable, DomainDTO {

    @Nullable
    private String id;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date startDate;

    @Nullable
    private Date endDate;

    @Nullable
    private Date createDate;

    @Nullable
    private Status status;

    @Nullable
    private String userId;

    @Override
    public String toString() {
        return "ProjectDTO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + EntityUtill.printDate(startDate) +
                ", endDate=" + EntityUtill.printDate(endDate) +
                ", createDate=" + EntityUtill.printDate(createDate) +
                ", status=" + status +
                ", userId='" + userId + '\'' +
                '}';
    }
}
