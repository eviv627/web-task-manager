package ru.evseenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.servlet.http.HttpSession;
import java.util.*;

public class HttpSessionRepository {

    private static Map<String, HttpSession> cache;

    public HttpSessionRepository() {
        if (cache == null) {
            cache = new HashMap<>();
        }
    }

    @Nullable
    public HttpSession getByKey(@NotNull final String key) {
        return cache.get(key);
    }

    @NotNull
    public List<HttpSession> getAll() {
        return new ArrayList<>(cache.values());
    }

    public void set(@NotNull final String key, @NotNull final HttpSession value) {
        cache.put(key, value);
    }

    public void remove(@NotNull final String key) {
        cache.remove(key);
    }

    public boolean contains(@NotNull final String sessionId) {
        return cache.containsKey(sessionId);
    }
}
