package ru.evseenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.dto.UserDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepository {

    private static Map<String, UserDTO> cache;

    public UserRepository() {
        if (cache == null) {
            cache = new HashMap<>();
        }
    }

    @Nullable
    public UserDTO getByKey(@NotNull final String key) {
        return cache.get(key);
    }

    @NotNull
    public List<UserDTO> getAll() {
        return new ArrayList<>(cache.values());
    }

    public void set(@NotNull final String key, @NotNull final UserDTO value) {
        cache.put(key, value);
    }

    @Nullable
    public UserDTO getByLogin(@NotNull final String login) {
        @NotNull final List<UserDTO> list = getAll();
        for (UserDTO user : list) {
            if (login.equals(user.getLogin())) {
                return user;
            }
        }
        return null;
    }
}
