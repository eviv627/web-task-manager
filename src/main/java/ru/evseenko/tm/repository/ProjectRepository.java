package ru.evseenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.dto.ProjectDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository {

    private static Map<String, ProjectDTO> cache = null;

    public ProjectRepository() {
        if (cache == null) {
            cache = new HashMap<>();
        }
    }

    @Nullable
    public ProjectDTO getByKey(@NotNull final String key) {
        return cache.get(key);
    }

    @NotNull
    public List<ProjectDTO> getAll() {
        return new ArrayList<>(cache.values());
    }

    public void set(@NotNull final String key, @NotNull final ProjectDTO value) {
        cache.put(key, value);
    }

    public void remove(@NotNull final String key) {
        cache.remove(key);
    }

    public boolean contains(@NotNull final String key) {
        return cache.containsKey(key);
    }
}
