package ru.evseenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.dto.TaskDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {

    private static Map<String, TaskDTO> cache;

    public TaskRepository() {
        if (cache == null) {
            cache = new HashMap<>();
        }
    }

    @Nullable
    public TaskDTO getByKey(@NotNull final String key) {
        return cache.get(key);
    }

    @NotNull
    public List<TaskDTO> getAll() {
        return new ArrayList<>(cache.values());
    }

    public void set(@NotNull final String key, @NotNull final TaskDTO value) {
        cache.put(key, value);
    }

    public void remove(@NotNull final String key) {
        cache.remove(key);
    }
}
