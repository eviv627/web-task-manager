package ru.evseenko.tm.util;

public class Constant {
    public static final String SESSION_USER_KEY = "SESSION_USER_KEY";
    public static final String UUID_REGEXP = "(\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b)";
}
