package ru.evseenko.tm.util;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateFormatUtil {

    public static final String ISO_DATE_REGEXP = "((19|20)[0-9][0-9]-(0[0-9]|1[0-2])-(0[1-9]|([12][0-9]|3[01]))T([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]\\+[0-9][0-9][0-9][0-9])";
    private static final SimpleDateFormat ISO_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    @NotNull
    public static Date parseIsoString(@NotNull final String date) {
        ISO_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("MSK"));

        @NotNull final Date dateObj;
        try {
            dateObj = ISO_DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException();
        }

        return dateObj;
    }

    @NotNull
    public static String parseIsoDate(@NotNull final Date date) {
        ISO_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("MSK"));
        @NotNull final String dateObj = ISO_DATE_FORMAT.format(date);
        return dateObj;
    }
}
