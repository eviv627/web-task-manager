package ru.evseenko.tm.api.entity;

public interface Identifiable {
    String getId();

    void setId(String id);
}
