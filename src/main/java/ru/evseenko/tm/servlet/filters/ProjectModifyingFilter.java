package ru.evseenko.tm.servlet.filters;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.dto.ProjectDTO;
import ru.evseenko.tm.repository.ProjectRepository;
import ru.evseenko.tm.util.Constant;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@NoArgsConstructor
@WebFilter(urlPatterns = {"/project/add*", "/project/edit*"})
public class ProjectModifyingFilter implements Filter {
    private ProjectRepository projectRepository;

    @Override
    public void init(FilterConfig filterConfig) {
        projectRepository = new ProjectRepository();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        @NotNull final HttpServletRequest servletRequest = (HttpServletRequest) request;
        @NotNull final HttpServletResponse servletResponse = (HttpServletResponse) response;

        @NotNull final HttpSession httpSession = servletRequest.getSession();
        @NotNull final String userId = (String) httpSession.getAttribute(Constant.SESSION_USER_KEY);
        @NotNull final String projectId = request.getParameter("id");


        if (projectId == null) {
            servletResponse.sendRedirect(String.format("%s%s", servletRequest.getContextPath(), "/project"));
            return;
        }

        @Nullable final ProjectDTO project = projectRepository.getByKey(projectId);

        if (project == null) {
            servletResponse.sendRedirect(String.format("%s%s", servletRequest.getContextPath(), "/project"));
            return;
        }

        assert project.getUserId() != null;
        if (!project.getUserId().equals(userId)) {
            servletResponse.sendRedirect(String.format("%s%s", servletRequest.getContextPath(), "/project"));
            return;
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
