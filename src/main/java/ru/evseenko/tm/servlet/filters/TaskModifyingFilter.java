package ru.evseenko.tm.servlet.filters;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.dto.TaskDTO;
import ru.evseenko.tm.repository.TaskRepository;
import ru.evseenko.tm.util.Constant;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@NoArgsConstructor
@WebFilter(urlPatterns = {"/task/add*", "/task/edit*"})
public class TaskModifyingFilter implements Filter {
    private TaskRepository taskRepository;

    @Override
    public void init(FilterConfig filterConfig) {
        taskRepository = new TaskRepository();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        @NotNull final HttpServletRequest servletRequest = (HttpServletRequest) request;
        @NotNull final HttpServletResponse servletResponse = (HttpServletResponse) response;

        @NotNull final HttpSession httpSession = servletRequest.getSession();
        @NotNull final String userId = (String) httpSession.getAttribute(Constant.SESSION_USER_KEY);
        @NotNull final String taskId = request.getParameter("id");


        if (taskId == null) {
            servletResponse.sendRedirect(String.format("%s%s", servletRequest.getContextPath(), "/task"));
            return;
        }

        @Nullable final TaskDTO task = taskRepository.getByKey(taskId);

        if (task == null) {
            servletResponse.sendRedirect(String.format("%s%s", servletRequest.getContextPath(), "/task"));
            return;
        }

        assert task.getUserId() != null;
        if (!task.getUserId().equals(userId)) {
            servletResponse.sendRedirect(String.format("%s%s", servletRequest.getContextPath(), "/task"));
            return;
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
