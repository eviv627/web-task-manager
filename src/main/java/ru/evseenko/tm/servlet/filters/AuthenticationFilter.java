package ru.evseenko.tm.servlet.filters;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.repository.HttpSessionRepository;
import ru.evseenko.tm.util.Constant;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@NoArgsConstructor
@WebFilter(urlPatterns = {"/project/*", "/task/*", "/user/*"})
public class AuthenticationFilter implements Filter {

    private HttpSessionRepository repository;

    @Override
    public void init(final FilterConfig filterConfig) {
        repository = new HttpSessionRepository();
    }

    @Override
    public void doFilter(
            final ServletRequest request,
            final ServletResponse response,
            final FilterChain chain
    ) throws IOException, ServletException {

        @NotNull final HttpServletRequest servletRequest = (HttpServletRequest) request;
        @NotNull final HttpServletResponse servletResponse = (HttpServletResponse) response;

        @NotNull final HttpSession httpSession = servletRequest.getSession();
        @Nullable final HttpSession actualHttpSession = repository.getByKey(httpSession.getId());

        if (actualHttpSession == null) {
            servletResponse.sendRedirect(String.format("%s%s", servletRequest.getContextPath(), "/"));
            return;
        }

        @Nullable final String userName = (String) httpSession.getAttribute(Constant.SESSION_USER_KEY);

        if (userName == null) {
            servletResponse.sendRedirect(String.format("%s%s", servletRequest.getContextPath(), "/"));
            return;
        }

        @NotNull final String actualUserName = (String) actualHttpSession.getAttribute(Constant.SESSION_USER_KEY);

        if (!userName.equals(actualUserName)) {
            servletResponse.sendRedirect(String.format("%s%s", servletRequest.getContextPath(), "/"));
            return;
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
