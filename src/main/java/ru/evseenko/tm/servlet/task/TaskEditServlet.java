package ru.evseenko.tm.servlet.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.Status;
import ru.evseenko.tm.entity.dto.TaskDTO;
import ru.evseenko.tm.repository.ProjectRepository;
import ru.evseenko.tm.repository.TaskRepository;
import ru.evseenko.tm.util.Constant;
import ru.evseenko.tm.util.DateFormatUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet("/task/edit")
public class TaskEditServlet extends HttpServlet {
    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        @NotNull final List<Status> statuses = new ArrayList<>();
        statuses.add(Status.PLANNED);
        statuses.add(Status.DONE);
        statuses.add(Status.IN_PROGRESS);
        req.setAttribute("statuses", statuses);
        req.setAttribute("datePattern", DateFormatUtil.ISO_DATE_REGEXP + "?");
        req.setAttribute("projectIdPattern", Constant.UUID_REGEXP + "?");

        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @Nullable final TaskDTO task = taskRepository.getByKey(req.getParameter("id"));
        if (task == null) {
            resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/task"));
            return;
        }
        req.setAttribute("task", task);
        if (task.getStartDate() != null) {
            req.setAttribute("taskStartDate", DateFormatUtil.parseIsoDate(task.getStartDate()));
        }
        if (task.getEndDate() != null) {
            req.setAttribute("taskEndDate", DateFormatUtil.parseIsoDate(task.getEndDate()));
        }

        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/task/edit.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        @NotNull final TaskRepository repository = new TaskRepository();
        @Nullable final TaskDTO task = repository.getByKey(req.getParameter("id"));
        if (task == null) {
            resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/task"));
            return;
        }

        task.setName(req.getParameter("name"));
        task.setDescription(req.getParameter("description"));
        task.setCreateDate(new Date());

        @Nullable final String startDateString = req.getParameter("startDate");
        if (startDateString != null && !"".equals(startDateString)) {
            task.setStartDate(DateFormatUtil.parseIsoString(startDateString));
        }

        @Nullable final String endDateString = req.getParameter("endDate");
        if (endDateString != null && !"".equals(endDateString)) {
            task.setEndDate(DateFormatUtil.parseIsoString(endDateString));
        }

        task.setStatus(Status.fromString(req.getParameter("statusName")));
        task.setProjectId(req.getParameter("projectId"));
        if (task.getProjectId() != null) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository();
            if (!projectRepository.contains(task.getProjectId())) {
                req.setAttribute("message", "No such project!");
                doGet(req, resp);
                return;
            }
        }

        assert task.getId() != null;
        repository.set(task.getId(), task);

        resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/task"));
    }
}
