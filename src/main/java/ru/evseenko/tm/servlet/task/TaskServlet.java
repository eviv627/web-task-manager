package ru.evseenko.tm.servlet.task;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.tm.entity.dto.TaskDTO;
import ru.evseenko.tm.repository.TaskRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/task")
public class TaskServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final List<TaskDTO> tasks = taskRepository.getAll();

        req.setAttribute("tasks", tasks);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/task/task.jsp");
        requestDispatcher.forward(req, resp);
    }
}
