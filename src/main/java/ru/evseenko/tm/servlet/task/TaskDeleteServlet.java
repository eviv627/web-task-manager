package ru.evseenko.tm.servlet.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.dto.TaskDTO;
import ru.evseenko.tm.repository.TaskRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/delete")
public class TaskDeleteServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        @NotNull final TaskRepository repository = new TaskRepository();
        @Nullable final TaskDTO task = repository.getByKey(req.getParameter("id"));
        if (task == null) {
            resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/task"));
            return;
        }

        assert task.getId() != null;
        repository.remove(task.getId());

        resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/task"));
    }
}
