package ru.evseenko.tm.servlet.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.dto.ProjectDTO;
import ru.evseenko.tm.repository.ProjectRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/delete")
public class ProjectDeleteServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        @NotNull final ProjectRepository repository = new ProjectRepository();
        @Nullable final ProjectDTO project = repository.getByKey(req.getParameter("id"));
        if (project == null) {
            resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/project"));
            return;
        }

        assert project.getId() != null;
        repository.remove(project.getId());

        resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/project"));
    }
}
