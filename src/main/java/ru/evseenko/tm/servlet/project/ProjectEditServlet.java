package ru.evseenko.tm.servlet.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.Status;
import ru.evseenko.tm.entity.dto.ProjectDTO;
import ru.evseenko.tm.repository.ProjectRepository;
import ru.evseenko.tm.util.DateFormatUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet("/project/edit")
public class ProjectEditServlet extends HttpServlet {
    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        @NotNull final List<Status> statuses = new ArrayList<>();
        statuses.add(Status.PLANNED);
        statuses.add(Status.DONE);
        statuses.add(Status.IN_PROGRESS);
        req.setAttribute("statuses", statuses);
        req.setAttribute("datePattern", DateFormatUtil.ISO_DATE_REGEXP + "?");

        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @Nullable final ProjectDTO project = projectRepository.getByKey(req.getParameter("id"));
        if (project == null) {
            resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/project"));
            return;
        }
        req.setAttribute("project", project);
        if (project.getStartDate() != null) {
            req.setAttribute("projectStartDate", DateFormatUtil.parseIsoDate(project.getStartDate()));
        }
        if (project.getEndDate() != null) {
            req.setAttribute("projectEndDate", DateFormatUtil.parseIsoDate(project.getEndDate()));
        }

        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/project/edit.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        @NotNull final ProjectRepository repository = new ProjectRepository();
        @Nullable final ProjectDTO project = repository.getByKey(req.getParameter("id"));
        if (project == null) {
            resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/project"));
            return;
        }

        project.setName(req.getParameter("name"));
        project.setDescription(req.getParameter("description"));
        project.setCreateDate(new Date());

        @Nullable final String startDateString = req.getParameter("startDate");
        if (startDateString != null && !"".equals(startDateString)) {
            project.setStartDate(DateFormatUtil.parseIsoString(startDateString));
        }

        @Nullable final String endDateString = req.getParameter("endDate");
        if (endDateString != null && !"".equals(endDateString)) {
            project.setEndDate(DateFormatUtil.parseIsoString(endDateString));
        }

        project.setStatus(Status.fromString(req.getParameter("statusName")));

        assert project.getId() != null;
        repository.set(project.getId(), project);

        resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/project"));
    }
}
