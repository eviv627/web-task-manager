package ru.evseenko.tm.servlet.project;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.tm.entity.dto.ProjectDTO;
import ru.evseenko.tm.repository.ProjectRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/project")
public class ProjectServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final List<ProjectDTO> projects = projectRepository.getAll();

        req.setAttribute("projects", projects);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/project/project.jsp");
        requestDispatcher.forward(req, resp);
    }
}
