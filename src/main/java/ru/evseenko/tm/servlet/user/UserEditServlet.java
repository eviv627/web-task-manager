package ru.evseenko.tm.servlet.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.dto.UserDTO;
import ru.evseenko.tm.repository.UserRepository;
import ru.evseenko.tm.util.Constant;
import ru.evseenko.tm.util.PasswordHashUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/user/edit")
public class UserEditServlet extends HttpServlet {

    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        @NotNull final HttpSession session = req.getSession();
        @NotNull final UserRepository repository = new UserRepository();
        @Nullable final UserDTO user = repository.getByKey((String) session.getAttribute(Constant.SESSION_USER_KEY));
        assert user != null;

        req.setAttribute("login", user.getLogin());
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/user/edit.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws IOException {
        @NotNull final HttpSession session = req.getSession();
        @NotNull final UserRepository repository = new UserRepository();
        @Nullable final UserDTO user = repository.getByKey((String) session.getAttribute(Constant.SESSION_USER_KEY));
        assert user != null;
        user.setLogin(req.getParameter("login"));
        user.setPasswordHash(PasswordHashUtil.md5(req.getParameter("password")));

        assert user.getId() != null;
        repository.set(user.getId(), user);

        resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/user"));
    }
}
