package ru.evseenko.tm.servlet.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.dto.UserDTO;
import ru.evseenko.tm.repository.HttpSessionRepository;
import ru.evseenko.tm.repository.UserRepository;
import ru.evseenko.tm.util.Constant;
import ru.evseenko.tm.util.PasswordHashUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class UserLoginServlet extends HttpServlet {
    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {

        @NotNull final HttpSessionRepository repository = new HttpSessionRepository();
        @NotNull final HttpSession session = req.getSession();

        if (repository.getByKey(session.getId()) != null) {
            resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/user"));
            return;
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/user/login.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        @NotNull final UserRepository repository = new UserRepository();
        @Nullable final UserDTO user = repository.getByLogin(req.getParameter("login"));
        if (user == null) {
            req.setAttribute("message", "incorrect login or password!");
            doGet(req, resp);
            return;
        }

        assert user.getPasswordHash() != null;
        if (!user.getPasswordHash().equals(
                PasswordHashUtil.md5(req.getParameter("password")
                ))) {
            req.setAttribute("message", "incorrect login or password!");
            doGet(req, resp);
            return;
        }

        @NotNull final HttpSession httpSession = req.getSession(true);
        httpSession.setAttribute(Constant.SESSION_USER_KEY, user.getId());

        @NotNull final HttpSessionRepository httpSessionRepository = new HttpSessionRepository();
        httpSessionRepository.set(httpSession.getId(), httpSession);

        resp.sendRedirect(String.format("%s%s", req.getContextPath(), "/user"));
    }
}
