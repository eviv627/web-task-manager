package ru.evseenko.tm.servlet.user;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.tm.entity.Role;
import ru.evseenko.tm.entity.dto.UserDTO;
import ru.evseenko.tm.repository.UserRepository;
import ru.evseenko.tm.util.PasswordHashUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/registration")
public class UserRegistrationServlet extends HttpServlet {

    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/user/registration.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        @NotNull final UserDTO user = new UserDTO();

        user.setLogin(req.getParameter("login"));
        user.setPasswordHash(PasswordHashUtil.md5(req.getParameter("password")));
        user.setRole(Role.ADMIN);
        user.setId(UUID.randomUUID().toString());

        @NotNull final UserRepository repository = new UserRepository();
        assert user.getId() != null;
        repository.set(user.getId(), user);

        req.setAttribute("message", "Registration success!");
        RequestDispatcher dispatcher = req.getRequestDispatcher("/");
        dispatcher.forward(req, resp);
    }
}
