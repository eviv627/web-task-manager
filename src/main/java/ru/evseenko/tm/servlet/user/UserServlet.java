package ru.evseenko.tm.servlet.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evseenko.tm.entity.dto.UserDTO;
import ru.evseenko.tm.repository.UserRepository;
import ru.evseenko.tm.util.Constant;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/user")
public class UserServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final HttpSession session = req.getSession();
        @NotNull final UserRepository repository = new UserRepository();
        @Nullable final UserDTO user = repository.getByKey((String) session.getAttribute(Constant.SESSION_USER_KEY));
        assert user != null;

        req.setAttribute("login", user.getLogin());
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/user/user.jsp");
        dispatcher.forward(req, resp);
    }

}
