package ru.evseenko.tm.servlet.user;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.tm.repository.HttpSessionRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/logout")
public class UserLogoutServlet extends HttpServlet {

    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        @NotNull final HttpSession session = req.getSession();
        @NotNull final HttpSessionRepository repository = new HttpSessionRepository();

        repository.remove(session.getId());
        session.invalidate();

        req.setAttribute("message", "logout!");
        RequestDispatcher dispatcher = req.getRequestDispatcher("/");
        dispatcher.forward(req, resp);
    }
}
