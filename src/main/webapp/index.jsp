<%@ page language="java" pageEncoding="UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<title>Main page</title>
</head>
<body>
<h1>Wellcome!</h1>
<form action="${pageContext.servletContext.contextPath}/login" method="POST">
	<p>
	    <a href="${pageContext.servletContext.contextPath}/login">
        <input type="button" value="Login" />
        </a>
    </p>
    <p>
        <a href="${pageContext.servletContext.contextPath}/registration">
            <input type="button" value="Registration" />
        </a>
    </p>
    <c:if test="${not empty message}">
        <p><b>${message}</b></p>
    </c:if>
</form>
</body>