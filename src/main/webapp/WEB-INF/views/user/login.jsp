<%@ page language="java" pageEncoding="UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<title>Login page</title>
</head>
<body>
<h1>Login :</h1>
<form action="${pageContext.servletContext.contextPath}/login" method="POST">
	<table>
		<tr>
			<td align="right" >Login : </td>
			<td>
				<input type="text" name="login" required>
			</td>
		</tr>
		<tr>
			<td align="right" >Password : </td>
			<td>
				<input type="password" name="password" required>
			</td>
		</tr>
		<tr>
			<td><input type="submit" value="Login"/></td>
		</tr>
	</table>
	<p>
	    <a href="${pageContext.servletContext.contextPath}/registration">
        <input type="button" value="Registration" />
        </a>
    </p>
    <c:if test="${not empty message}">
        <p><b>${message}</b></p>
    </c:if>
</form>
</body>
