<%@ page language="java" pageEncoding="UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<title>User status page</title>
</head>
<body>
    <p>
        Your login : ${login}
    </p>
    <p>
        <a href="${pageContext.servletContext.contextPath}/user/edit">
            <input type="button" value="Edit" />
        </a>
    </p>
    <p>
            <a href="${pageContext.servletContext.contextPath}/logout">
                <input type="button" value="Logout" />
            </a>
        </p>
	<p>
       <a href="${pageContext.servletContext.contextPath}/project">
                <input type="button" value="Projects" />
       </a>
    </p>
    <p>
       <a href="${pageContext.servletContext.contextPath}/task">
                    <input type="button" value="Tasks" />
       </a>
    </p>
</body>
</html>
