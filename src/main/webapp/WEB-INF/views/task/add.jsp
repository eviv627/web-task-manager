<%@ page language="java" pageEncoding="UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<title>Add task</title>
</head>
<body>
<h1>Add task :</h1>
    <c:if test="${not empty message}">
        <p><b>${message}</b></p>
    </c:if>
<form action="${pageContext.servletContext.contextPath}/task/add" method="POST">
	<table>
		<tr>
			<td align="right" >Task name : </td>
			<td>
				<input type="text" name="name" required>
			</td>
		</tr>
		<tr>
            <td align="right" >Task description : </td>
        	<td>
        	    <input type="text" name="description" required>
        	</td>
        </tr>
		<tr>
			<td align="right" >Task start date : </td>
			<td>
				<input name="startDate" pattern=${datePattern}
				    value="yyyy-MM-dd'T'HH:mm:ss+zzzz">
			</td>
		</tr>
		<tr>
        	<td align="right" >Task end date : </td>
        	<td>
        	    <input name="endDate" pattern=${datePattern}
        		    value="yyyy-MM-dd'T'HH:mm:ss+zzzz">
        	</td>
        </tr>
        <tr>
            <td align="right" >Parent project Id : </td>
                <td>
                    <input name="projectId" pattern=${projectIdPattern}
                	    value="">
                </td>
        </tr>
        <tr>
        	<td align="right" >Status : </td>
        	<td>
        	    <select required name="statusName">
            	<option disabled>Select status</option>
            	<c:forEach items="${statuses}" var="status" varStatus="statusNum">
            	    <option value="${status.name}">${status.name}</option>
            	</c:forEach>
           		</select>
           	<tr>
		<tr>
			<td><input type="submit" value="Submit"/></td>
		</tr>
	</table>
</form>
</body>
