<%@ page language="java" pageEncoding="UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<title>Add task</title>
</head>
<body>
<h1>Add task :</h1>
    <c:if test="${not empty message}">
        <p><b>${message}</b></p>
    </c:if>
<form action="${pageContext.servletContext.contextPath}/task/edit" method="POST">
    <input type="hidden" name="id" value=${task.id}>
	<table>
		<tr>
			<td align="right" >Task name : </td>
			<td>
				<input type="text" name="name" required
				value=${task.name}>
			</td>
		</tr>
		<tr>
            <td align="right" >Task description : </td>
        	<td>
        	    <input type="text" name="description" required
        	    value=${task.description}>
        	</td>
        </tr>
		<tr>
		    <td align="right" >Task start date : </td>
			<td>
                <c:set var="startDate" scope="session" value="yyyy-MM-dd'T'HH:mm:ss+zzzz" />
                <c:if test="${not empty taskStartDate}">
                    <c:set var="startDate" scope="session" value="${taskStartDate}" />
                </c:if>
                <input name="startDate" pattern=${datePattern}
                    value=${startDate}>
            </td>
		</tr>
		<tr>
        	<td align="right" >Task end date : </td>
        	<td>
        	    <c:set var="endDate" scope="session" value="yyyy-MM-dd'T'HH:mm:ss+zzzz" />
        	    <c:if test="${not empty taskEndDate}">
                    <c:set var="endDate" scope="session" value="${taskEndDate}" />
                </c:if>
                <input name="endDate" pattern=${datePattern}
                    value=${endDate}>
        	</td>
        </tr>
        <tr>
            <td align="right" >Parent project Id : </td>
            <c:if test="${not empty task.projectId}">
                <c:set var="projectId" scope="session" value="${task.projectId}" />
            </c:if>
            <td>
                <input name="projectId" pattern=${projectIdPattern}
                    value=${projectId}>
            </td>
        </tr>
        <tr>
        	<td align="right" >Status : </td>
        	<td>
        	    <select required name="statusName">
            	<option disabled>Select status</option>
            	<c:forEach items="${statuses}" var="status" varStatus="statusNum">
            	    <option value="${status.name}">${status.name}</option>
            	</c:forEach>
           		</select>
           	<tr>
		<tr>
			<td><input type="submit" value="Submit"/></td>
		</tr>
	</table>
</form>
</body>